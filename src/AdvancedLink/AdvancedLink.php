<?php

namespace Solnet\AdvancedLink;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Dev\Debug;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\View\Requirements;

class AdvancedLink extends DataObject
{
    use Configurable;

    private static $table_name = 'AdvancedLink';
    private static $singular_name = 'Link';
    private static $plural_name = 'Links';

    private static $db = [
        'LinkType' => 'Enum("Internal,External,File,Phone,Email","Internal")',
        'Link' => 'Text',
        'TargetBlank' => 'Boolean',
        'CTAText' => 'Varchar(100)',
        'ButtonStyleClass' => 'Varchar',
    ];

    private static $has_one = [
        'Page' => 'SilverStripe\CMS\Model\SiteTree',
        'File' => 'SilverStripe\Assets\File'
    ];

    private static $button_styles = [
        'btn-primary' => 'ash blue button, golden brown hover',
        'btn-light' => 'turquoise button, dark blue hover',
        'btn-secondary' => 'dark blue button, turquoise hover',
        'btn-outline-primary' => 'turquoise button, turquoise outline hover',
        'btn-outline-secondary' => 'dark blue button, dark blue outline hover'
    ];

    private static $summary_fields = [
        'CTAText' => 'CTAText'
    ];

    private static $defaults = [
        'LinkType' => 'Internal'
    ];

    public function getCMSFields()
    {
        Requirements::javascript('resources/solnet/silverstripe-advancedlink/client/dist/js/AdvancedLinkCMS.js');

        $buttonStyles = Config::inst()->get('Solnet\AdvancedLink\AdvancedLink', 'button_styles');

        $fields = parent::getCMSFields();

        $fields->removeByName(array('CTAText', 'TargetBlank'));

        $fields->addFieldsToTab(
            'Root.Main',
            [
                TreeDropdownField::create(
                    'PageID',
                    'Page',
                    SiteTree::class
                ),
                TextField::create(
                    'CTAText',
                    _t('AdvancedLink.CTAText_Title', 'CTA Text')
                ),
                DropdownField::create(
                    'ButtonStyleClass',
                    _t('AdvancedLink.ButtonStyleClass_Title', 'Button Style'),
                    $buttonStyles
                )
                ->setEmptyString('Not a button'),
                CheckboxField::create(
                    'TargetBlank',
                    _t('AdvancedLink.TargetBlank_Title', 'Open in new tab?')
                ),
                OptionsetField::create(
                    'LinkType',
                    _t('AdvancedLink.Type_Title', 'Type'),
                    $this->dbObject('LinkType')->enumValues()
                ),
                TextField::create(
                    'Link',
                    _t('AdvancedLink.Link_Title', 'External Link')
                )
                ->setDescription(
                    _t(
                        'AdvancedLink.Link_Description',
                        'If type is phone or email, the link will open with the default application for handling those. eg Skype or Outlook depending on the settings in your OS.'
                    )
                ),
                UploadField::create(
                    'File',
                    _t('AdvancedLink.File_Title', 'File')
                )
            ]
        );

        return $fields;
    }

    public function URL()
    {
        $link = '#';
        if ($this->LinkType == 'Internal') {
            if ($this->PageID > 0) {
                $link = $this->Page()->Link();
            }
        } elseif ($this->LinkType == 'File') {
            if ($this->FileID != '') {
                $link = $this->File()->Link();
            }
        } elseif ($this->Link != '') {
            if($this->LinkType == 'External'){
                $link = $this->Link;
            }
            elseif($this->LinkType == 'Phone'){
                $link = 'tel:' . $this->Link;
            }
            elseif($this->LinkType == 'Email'){
                $link = 'mailto:' . $this->Link;
            }
            else{
                $link = $this->Link;
            }
        }

        return $link;
    }

    public function NewTab(){
        $newTab = $this->TargetBlank;
        if($this->LinkType == 'File'){
            $newTab = true;
        }
        elseif($this->LinkType == 'Phone'){
            $newTab = false;
        }
        elseif($this->LinkType == 'Email'){
            $newTab = false;
        }

        return $this->TargetBlank ? true : false;
    }

    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        if($this->File()->exists()){
            $this->File()->doPublish();
        }
    }
}
